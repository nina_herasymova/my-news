<?php
$doc = new DOMDocument;
$content_rss = file_get_contents('https://www.liga.net/news/interview/rss.xml');
$items_rss = new SimpleXMLElement($content_rss);

foreach ($items_rss->channel->item as $key => $item_rss) {

  $picture = $item_rss->enclosure;
  $picture_src = $picture->attributes()['url'];
  echo "<img src = '$picture_src' >";

  echo '<p style = "color: #0f1111; font-size: 30px; font-weight: bold; font-family: sans-serif; margin-bottom: 0">' . $item_rss->title . '</p> ' . '<br>';

  echo '<p style = "font-size: 20px; font-family: sans-serif">' . $item_rss->description . '</p> ';

  $res = preg_replace('/www\.liga\.net/', "www.my-news.com", $item_rss->link);
  echo '<p style = "font-family: sans-serif; padding-bottom: 20px; border-bottom: 2px solid #c8c5c5">' . 'Подробнее по ссылке: '. '<a ' . 'href="' . $item_rss->link . '">' . $res . '</a>' . '</p>'. '<br>';

}